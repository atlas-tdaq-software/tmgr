/**
*	@file:		tmgr/tmresult.h                                              
*	@author:	Robert Hart, NIKHEF, Amsterdam				
*	Author:	Andrei Kazarov						
*	Date:	February 2001						
*
*	Modified:							
*		- Feb 2001 Derived from original tmgr.h                 
*		- Feb 2003 Added TIMEOUT test result    (AK)            
*/

#ifndef	TMRESULT_H
#define	TMRESULT_H

/************************************************************************
 POSIX 1003.3 result definitions.
 test binaries can return:
 				TM_PASS		test confer the tested functionality
				TM_FAIL		Functionality 
				TM_UNRESOLVED   Can not judge about functionality (internal test problems, lack of resources,
										   system errors, timeout etc.)

 TM_Client can additionally return
				TM_UNTESTED	test was not started by whatever reason
				TM_UNSUPPORTED  ? (test was not found in the repository)
				TM_UNDEF	initial value, before test is started
************************************************************************/

namespace daq	{
namespace tmgr 	{

   /**
   * The possible values returned as single test result by test manager
   * TmPass, TmFail, TmUnresolved can be returned by the test binaries.
   * TmUntested, TmUnsupported can be returned by test manager.
   */
enum	TestResult
{
	TmPass        = 0,   ///<test completed and confirmed the tested functionality
	TmUndef       = 182, ///<initial value 
	TmFail        = 183, ///<test completed and confirmed that component is not working properly
	TmUnresolved  = 184, ///<test completed but it could not verify the functionality due to internal issues
	TmUntested    = 185, ///<test was not executed, e.g. some problems with test manager itself or with test configuration
	TmUnsupported = 186  ///<test was not executed (e.g. component is untestable) - in practive TmUntested is returned in this case
};

}} // namespace daq::tmgr

#endif
